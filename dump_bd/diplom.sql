-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 14 2017 г., 18:40
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `diplom`
--

-- --------------------------------------------------------

--
-- Структура таблицы `dp_catalog_settings`
--

CREATE TABLE IF NOT EXISTS `dp_catalog_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(255) NOT NULL COMMENT 'Опция',
  `value` text NOT NULL COMMENT 'Значение',
  `name` varchar(255) NOT NULL COMMENT 'Опция (uppercase)',
  `activity` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Включена',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `dp_category`
--

CREATE TABLE IF NOT EXISTS `dp_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT 'Загаловок',
  `url` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID родителя  [0-нет родителя]',
  `parent_url` varchar(255) NOT NULL COMMENT 'URL родителя',
  `sort` int(11) NOT NULL COMMENT 'Сортировка',
  `html_content` longtext NOT NULL COMMENT 'HTML контент',
  `menu_display` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Выводить в меню',
  `image_url` text NOT NULL COMMENT 'Картинка категории',
  `activity` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Активна',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `dp_category`
--

INSERT INTO `dp_category` (`id`, `title`, `url`, `parent_id`, `parent_url`, `sort`, `html_content`, `menu_display`, `image_url`, `activity`) VALUES
(1, 'Телефоны и связь', '', 0, '', 1, '', 1, '', 1),
(2, 'Планшеты и ридеры', '', 0, '', 2, '', 1, '', 1),
(3, 'Ноутбуки и компьютеры', '', 0, '', 3, '', 1, '', 1),
(4, 'Фото- и видеокамеры', '', 0, '', 4, '', 1, '', 1),
(5, 'Аудио, видео', '', 0, '', 5, '', 1, '', 1),
(6, 'Смартфоны', '', 1, '', 8, '', 1, '', 1),
(7, 'Аксессуары', '', 1, '', 7, '', 1, '', 1),
(8, 'Телефоны', '', 1, '', 6, '', 1, '', 1),
(9, 'Ноутбуки', '', 3, '', 9, '', 1, '', 1),
(10, 'Компьютеры', '', 3, '', 10, '', 1, '', 1),
(11, 'Периферийные устройства', '', 3, '', 11, '', 1, '', 1),
(12, 'Мониторы', '', 11, '', 12, '', 1, '', 1),
(13, 'Клавиатуры', '', 11, '', 13, '', 1, '', 1),
(14, 'Мышки', '', 11, '', 14, '', 1, '', 1),
(15, 'Смартфоны Apple', '', 6, '', 15, '', 1, '', 1),
(16, 'Смартфоны Samsung', '', 6, '', 16, '', 1, '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_category_user_property`
--

CREATE TABLE IF NOT EXISTS `dp_category_user_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT 'ID Категории (ссылка)',
  `name` varchar(255) NOT NULL COMMENT 'Название свойства',
  `title_property_group_id` int(11) NOT NULL COMMENT 'ID Группы свойств',
  `main_property` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Основное свойство',
  `type_view` enum('string','checkbox','select','radiobutton') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `dp_category_user_property`
--

INSERT INTO `dp_category_user_property` (`id`, `category_id`, `name`, `title_property_group_id`, `main_property`, `type_view`) VALUES
(5, 6, 'Тип дисплея', 1, 0, 'string'),
(6, 6, 'Диагональ (дюйм)', 1, 1, 'string'),
(7, 6, 'Разрешение (пикс)', 1, 1, 'string'),
(8, 6, 'Сенсорный дисплей', 1, 0, 'radiobutton'),
(9, 6, 'Поддержка Multitouch', 1, 0, 'radiobutton'),
(10, 6, 'Фотокамера (Мп)', 2, 1, 'string'),
(11, 6, 'Разрешение фотосъемки (пикс)', 2, 0, 'string'),
(12, 6, 'Автофокус', 2, 0, 'radiobutton'),
(13, 6, 'Вспышка', 2, 0, 'radiobutton'),
(14, 6, 'Фронтальная камера (Мп)', 2, 0, 'string'),
(15, 6, 'Встроенная память (Гб)', 4, 1, 'string'),
(16, 6, 'Оперативная память (Мб)', 4, 0, 'string'),
(17, 6, 'Поддержка карт памяти', 4, 0, 'radiobutton');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_component`
--

CREATE TABLE IF NOT EXISTS `dp_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Имя компонента',
  `url` varchar(255) NOT NULL COMMENT 'URL компонента',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `dp_component`
--

INSERT INTO `dp_component` (`id`, `name`, `url`) VALUES
(1, 'Каталог', '/catalog.php'),
(2, 'Категории', '/category.php'),
(3, 'Меню', '/menu.php'),
(4, 'Продукт', '/product.php'),
(5, 'Контент', '/content.php'),
(6, 'Название сайта', '/site_name.php'),
(7, 'Авторизация', '/auth.php'),
(8, 'Малая корзина', '/basket_small.php'),
(9, 'Большая корзина', '/basket_big.php'),
(10, 'Оформление заказа', '/order.php');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_currency`
--

CREATE TABLE IF NOT EXISTS `dp_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `reduction` varchar(20) NOT NULL COMMENT 'Сокращение',
  `ico` text NOT NULL COMMENT 'Иконка',
  `ISO` varchar(10) NOT NULL COMMENT 'Абривиатура',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `dp_currency`
--

INSERT INTO `dp_currency` (`id`, `name`, `reduction`, `ico`, `ISO`) VALUES
(1, 'Рубли', 'руб.', '', 'RUR'),
(2, 'Доллары', '$', '', 'USD'),
(3, 'Евро', '€', '', 'EUR');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_delivery`
--

CREATE TABLE IF NOT EXISTS `dp_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `cost` double NOT NULL COMMENT 'Стоимость',
  `html_content` text NOT NULL COMMENT 'Описание',
  `sort` int(11) NOT NULL COMMENT 'Сортировка',
  `image_url` text NOT NULL COMMENT 'Изображение',
  `activity` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Включен',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `dp_delivery`
--

INSERT INTO `dp_delivery` (`id`, `name`, `cost`, `html_content`, `sort`, `image_url`, `activity`) VALUES
(1, 'Самовывоз', 0, 'Самовывоз из пункта выдачи товаров', 1, '', 1),
(2, 'Доставка курьером', 300, 'Доставка курьером в пределах г.Ижевска', 2, '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_layout`
--

CREATE TABLE IF NOT EXISTS `dp_layout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL COMMENT 'URL макета страницы',
  `quantity_component` int(2) NOT NULL COMMENT 'Колличество компонентов макета',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `dp_layout`
--

INSERT INTO `dp_layout` (`id`, `url`, `quantity_component`) VALUES
(1, '/default.php', 2),
(2, '/catalog.php', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_layout_component`
--

CREATE TABLE IF NOT EXISTS `dp_layout_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loyout_id` int(11) NOT NULL COMMENT 'ID Шаблона страницы',
  `component_id` int(11) NOT NULL COMMENT 'ID компонента',
  `component_settings` longtext NOT NULL COMMENT 'Настройки компонента (Сериализация)',
  `component_cell` int(2) NOT NULL COMMENT 'Номер ячейки компонента',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `dp_layout_component`
--

INSERT INTO `dp_layout_component` (`id`, `loyout_id`, `component_id`, `component_settings`, `component_cell`) VALUES
(1, 1, 6, '', 1),
(2, 1, 3, '', 2),
(3, 1, 7, '', 3),
(4, 1, 8, '', 4),
(5, 1, 5, '', 5),
(6, 2, 6, '', 1),
(7, 2, 3, '', 2),
(8, 2, 7, '', 3),
(9, 2, 8, '', 4),
(10, 2, 2, '', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_meta`
--

CREATE TABLE IF NOT EXISTS `dp_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL COMMENT 'ID Страницы (ссылка)',
  `url` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `meta_keywords` text NOT NULL COMMENT 'Ключевые слова',
  `meta_desc` text NOT NULL COMMENT 'Описание',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `dp_order`
--

CREATE TABLE IF NOT EXISTS `dp_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(32) NOT NULL COMMENT 'Номер заказа',
  `user_id` int(11) NOT NULL COMMENT 'ID Пользователя',
  `order_date` date NOT NULL COMMENT 'Дата заказа',
  `order_status` int(1) NOT NULL COMMENT 'Текущий статус заказа  (цифрой) (ссылка)',
  `order_content` longtext NOT NULL COMMENT 'Содержимое заказа (Сериализация)',
  `order_summ` double NOT NULL COMMENT 'Общая сумма заказа',
  `delivery_id` int(11) NOT NULL COMMENT 'ID Способа доставки',
  `delivery_cost` double NOT NULL COMMENT 'Стоимость доставки',
  `payment_id` int(11) NOT NULL COMMENT 'ID Способа оплаты',
  `comment` text NOT NULL COMMENT 'Пользовательский комментарий',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `dp_order`
--

INSERT INTO `dp_order` (`id`, `order_number`, `user_id`, `order_date`, `order_status`, `order_content`, `order_summ`, `delivery_id`, `delivery_cost`, `payment_id`, `comment`) VALUES
(1, '', 4, '2017-03-14', 1, 'a:1:{i:2;a:3:{s:7:"product";s:1:"2";s:5:"price";s:5:"27990";s:5:"count";i:1;}}', 27990, 2, 300, 1, ''),
(2, '', 4, '2017-03-14', 2, 'a:1:{i:5;a:3:{s:7:"product";s:1:"5";s:5:"price";s:5:"37000";s:5:"count";i:1;}}', 37000, 1, 0, 2, ''),
(3, '', 4, '2017-03-14', 3, 'a:1:{i:5;a:3:{s:7:"product";s:1:"5";s:5:"price";s:5:"37000";s:5:"count";i:1;}}', 37000, 1, 0, 2, ''),
(4, '', 4, '2017-03-14', 4, 'a:1:{i:5;a:3:{s:7:"product";s:1:"5";s:5:"price";s:5:"37000";s:5:"count";i:1;}}', 37000, 2, 300, 2, ''),
(5, '', 4, '2017-03-14', 5, 'a:1:{i:1;a:3:{s:7:"product";s:1:"1";s:5:"price";s:5:"60000";s:5:"count";i:1;}}', 60000, 2, 300, 2, 'мой заказ'),
(6, '', 4, '2017-03-14', 6, 'a:1:{i:3;a:3:{s:7:"product";s:1:"3";s:5:"price";s:5:"50000";s:5:"count";i:2;}}', 100000, 2, 300, 1, 'олтотлото');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_order_status`
--

CREATE TABLE IF NOT EXISTS `dp_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL COMMENT 'ID Заказа (ссылка)',
  `order_status` int(1) NOT NULL DEFAULT '0' COMMENT 'Статус заказа [0-не подтвержден, 1-ожидает оплаты, 2-оплачен, 3-в доставке, 4-отменен, 5-выполнен, 6-в обработке]',
  `order_status_date` date NOT NULL COMMENT 'Дата установки статуса',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `dp_order_status`
--

INSERT INTO `dp_order_status` (`id`, `order_id`, `order_status`, `order_status_date`) VALUES
(1, 1, 0, '2017-03-14'),
(2, 2, 0, '2017-03-14'),
(3, 3, 0, '2017-03-14'),
(4, 4, 0, '2017-03-14'),
(5, 5, 0, '2017-03-14'),
(6, 6, 0, '2017-03-14');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_page`
--

CREATE TABLE IF NOT EXISTS `dp_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID родителя  [0-нет родителя]',
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `html_content` longtext NOT NULL COMMENT 'HTML контент',
  `sort` int(11) NOT NULL COMMENT 'Сортировка',
  `loyout_id` int(11) NOT NULL DEFAULT '1' COMMENT 'ID Макета страницы (по умолчанию 0 -Default)',
  `menu_display` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Выводить в меню',
  `activity` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `dp_page`
--

INSERT INTO `dp_page` (`id`, `url`, `parent_id`, `title`, `html_content`, `sort`, `loyout_id`, `menu_display`, `activity`) VALUES
(1, '/', 0, 'Главная', 'Контент главной страницы\nКаждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. ', 1, 1, 1, 1),
(2, '/catalog/index.php', 0, 'Каталог', 'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. ', 2, 2, 1, 1),
(3, '/delivery_and_payment.php', 0, 'Доставка и оплата', 'Контент страницы доставка и оплата\nКаждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. ', 3, 1, 1, 1),
(4, '', 0, 'Акции', 'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. ', 4, 1, 1, 1),
(5, '', 4, 'Распродажа', 'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. ', 5, 1, 1, 1),
(6, '', 4, 'Акции', 'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. ', 6, 1, 1, 1),
(7, '', 4, 'Бонусы', 'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. ', 7, 1, 1, 1),
(8, '', 0, 'Продукт', 'мывывмывмывмв', 7, 2, 0, 1),
(9, '/basket.php', 0, 'Корзина', 'В корзине отображаются товары, выбранные для оформления заказа', 8, 1, 0, 1),
(10, '/order.php', 0, 'Оформление заказа', '', 9, 1, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_page_component`
--

CREATE TABLE IF NOT EXISTS `dp_page_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL COMMENT 'ID страницы',
  `component_id` int(11) NOT NULL COMMENT 'ID компонента',
  `component_settings` longtext NOT NULL COMMENT 'Настройки компонента (Сериализация)',
  `component_cell` int(2) NOT NULL COMMENT 'Номер ячейки компонента',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `dp_page_component`
--

INSERT INTO `dp_page_component` (`id`, `page_id`, `component_id`, `component_settings`, `component_cell`) VALUES
(1, 2, 1, '', 6),
(6, 8, 4, '', 6),
(7, 9, 9, '', 5),
(8, 10, 10, '', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_payment`
--

CREATE TABLE IF NOT EXISTS `dp_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `currency_id` int(11) NOT NULL COMMENT 'id валюты (ссылка)',
  `paramArray` text NOT NULL COMMENT 'Массив параметров',
  `urlArray` text NOT NULL COMMENT 'Массив URL',
  `sort` int(11) NOT NULL COMMENT 'Сортировка',
  `image_url` text NOT NULL COMMENT 'Изображение',
  `activity` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Включен',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `dp_payment`
--

INSERT INTO `dp_payment` (`id`, `name`, `currency_id`, `paramArray`, `urlArray`, `sort`, `image_url`, `activity`) VALUES
(1, 'Наличный расчет', 1, '', '', 1, '', 1),
(2, 'Оплата платежной картой при получении заказа', 1, '', '', 2, '', 1),
(3, 'Онлайн-оплата платежной картой VISA и MasterCard', 1, '', '', 3, '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_product`
--

CREATE TABLE IF NOT EXISTS `dp_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number_code` varchar(255) NOT NULL COMMENT 'Код Артикул',
  `sort` int(11) NOT NULL COMMENT 'Сортировка',
  `category_id` int(11) NOT NULL COMMENT 'ID Категории',
  `title` varchar(255) NOT NULL COMMENT 'Загаловок',
  `html_content` longtext NOT NULL COMMENT 'Описание',
  `price` double NOT NULL COMMENT 'Цена',
  `old_price` double NOT NULL COMMENT 'Старая цена',
  `purchase_price` double NOT NULL COMMENT 'Закупочная цена',
  `currency_id` int(11) NOT NULL COMMENT 'Валюта',
  `url` varchar(255) NOT NULL,
  `image_main_id` int(11) NOT NULL COMMENT 'ID Главного изображения',
  `count` int(11) NOT NULL DEFAULT '-1' COMMENT 'Количество  (-1 - не ограничено)',
  `new` tinyint(1) NOT NULL COMMENT 'Новинка',
  `related` text NOT NULL COMMENT 'Список рекомендуемых товаров',
  `count_buy` int(11) NOT NULL DEFAULT '0' COMMENT 'Колличество проданных товаров',
  `activity` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `dp_product`
--

INSERT INTO `dp_product` (`id`, `number_code`, `sort`, `category_id`, `title`, `html_content`, `price`, `old_price`, `purchase_price`, `currency_id`, `url`, `image_main_id`, `count`, `new`, `related`, `count_buy`, `activity`) VALUES
(1, 'К15Т1', 3, 15, 'Смартфон Apple iPhone 7 128GB', '<strong>ЭТО 7</strong> \n<br>\n \n<br>\n В iPhone 7 все важнейшие аспекты iPhone значительно улучшены. Это принципиально новая система камер для фото и видеосъемки. Максимально мощный и экономичный аккумулятор. Стереодинамики с богатым звучанием. Самый яркий и разноцветный из всех дисплеев iPhone. Защита от брызг и воды. И его внешние данные впечатляют не менее, чем внутренние возможности. Все это iPhone 7. ', 60000, 65000, 0, 1, '', 1, 40, 0, '', 0, 1),
(2, 'К16Т2', 5, 16, 'Смартфон Samsung Galaxy A5', '<strong>СТИЛЬНЫЙ И МИНИМАЛИСТИЧНЫЙ</strong>\n\n<br>\nСовременный минималистичный корпус из 3D-стекла и металла, а также 5,2-дюймовый экран Full HD sAMOLED – все это отличительные черты Samsung Galaxy A5 (2017).\n<br>\n\n<br>\n<strong>УДОБНЫЙ И ЭРГОНОМИЧНЫЙ</strong>\n<br>\n\n<br>\nПлавные линии корпуса, отсутствие выступов камеры, утонченная и элегантная отделка позволяют получить настоящее удовольствие от использования смартфона.', 27990, 30000, 0, 1, '', 2, -1, 0, '', 0, 1),
(3, 'К15Т2', 2, 15, 'Смартфон Apple iPhone 7 64GB', '<strong>ЭТО 7</strong> \n<br>\n В iPhone 7 все важнейшие аспекты iPhone значительно улучшены. Это принципиально новая система камер для фото и видеосъемки. Максимально мощный и экономичный аккумулятор. Стереодинамики с богатым звучанием. Самый яркий и разноцветный из всех дисплеев iPhone. Защита от брызг и воды. И его внешние данные впечатляют не менее, чем внутренние возможности. Все это iPhone 7. ', 50000, 55000, 0, 1, '', 3, -1, 0, '', 0, 1),
(4, 'К15Т3', 2, 15, 'Смартфон Apple iPhone 7 32GB', '<strong>ЭТО 7</strong> \n<br>\n В iPhone 7 все важнейшие аспекты iPhone значительно улучшены. Это принципиально новая система камер для фото и видеосъемки. Максимально мощный и экономичный аккумулятор. Стереодинамики с богатым звучанием. Самый яркий и разноцветный из всех дисплеев iPhone. Защита от брызг и воды. И его внешние данные впечатляют не менее, чем внутренние возможности. Все это iPhone 7.', 40000, 45000, 0, 1, '', 4, -1, 0, '', 0, 1),
(5, 'K15T4', 1, 15, 'Смартфон Apple iPhone SE 64GB', '<strong>МАЛАЯ ФОРМА В СВОЕЙ ЛУЧШЕЙ ФОРМЕ</strong> \n<br>\n Представляем iPhone SE – самый мощный 4‑дюймовый смартфон в истории. Чтобы создать его, в Apple взяли за основу полюбившийся дизайн и полностью поменяли содержание. Установили тот же передовой процессор A9, что и на iPhone 6s, и камеру 12 Мп для съемки невероятных фотографий и видео 4K. А благодаря Live Photos любой ваш снимок буквально оживет. Результат? Небольшой iPhone с огромными возможностями. ', 37000, 39000, 0, 1, '', 5, -1, 0, '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_product_image`
--

CREATE TABLE IF NOT EXISTS `dp_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL COMMENT 'ID Товара (ссылка)',
  `image_url` text NOT NULL COMMENT 'Путь к картинке',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `dp_product_image`
--

INSERT INTO `dp_product_image` (`id`, `product_id`, `image_url`) VALUES
(1, 1, '/img/uploads/product/1/1.jpg'),
(2, 2, '/img/uploads/product/2/1.jpg'),
(3, 3, '/img/uploads/product/3/1.jpg'),
(4, 4, '/img/uploads/product/4/1.jpg'),
(5, 5, '/img/uploads/product/5/1.jpg'),
(6, 1, '/img/uploads/product/1/2.jpg'),
(7, 1, '/img/uploads/product/1/3.jpg'),
(8, 1, '/img/uploads/product/1/4.jpg'),
(9, 1, '/img/uploads/product/1/5.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_product_user_property`
--

CREATE TABLE IF NOT EXISTS `dp_product_user_property` (
  `category_user_property_id` int(11) NOT NULL COMMENT 'ID Пользовательского свойства категории (ссылка)',
  `product_id` int(11) NOT NULL COMMENT 'ID Товара (ссылка)',
  `value` text NOT NULL COMMENT 'Значение свойства',
  PRIMARY KEY (`category_user_property_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dp_product_user_property`
--

INSERT INTO `dp_product_user_property` (`category_user_property_id`, `product_id`, `value`) VALUES
(5, 1, 'Retina HD'),
(6, 1, '4.7'),
(7, 1, '1334x750'),
(8, 1, 'да'),
(9, 1, 'да'),
(10, 1, '8'),
(11, 1, '3248 x 2448'),
(12, 1, 'да'),
(13, 1, 'да'),
(14, 1, '1.2'),
(15, 1, '16'),
(16, 1, '1024'),
(17, 1, 'нет');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_review`
--

CREATE TABLE IF NOT EXISTS `dp_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL COMMENT 'ID Товара',
  `review_date` date NOT NULL,
  `author` varchar(255) NOT NULL COMMENT 'Имя автора отзыва',
  `review` text NOT NULL COMMENT 'Отзыв',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `dp_review`
--

INSERT INTO `dp_review` (`id`, `product_id`, `review_date`, `author`, `review`) VALUES
(1, 1, '2017-02-13', 'Владимир', 'батарея перестает работать, на холоде при батарее +50% внезапно выключается , тоже самое при жаре. Экран просто нужно беречь как зеницу ока, не дай бог он у Вас где-нибудь упадет - весь рассыпется. Очень неудобно использовать из-за размера, постоянно нужно вытягивать большой палец. Телефон не стоит своих денег. По времени служит так же как и обычный телефон и при этом он не практичный, хрупкий, неудобный. Не советую и не рекомендую. Один дизайн нормальный вот и все!'),
(2, 1, '2016-12-22', 'Виктор', 'Экран стал намного больше, при этом несмотря на значительное прибавление в объемах по сравнению с Samsung не кажется таким громоздким и достаточно удобен в использовании (хорошо лежит в руках) + увеличилось качество фотографий + touch ID определенно хорошая штука, удобно для разблокировки телефона и дополнительная мера безопасности + сохранность от всяких вирусных программ + легкость и простота в использовании что пресуще всем "яблочным " гаджетам , по сути есть только 2 кнопки, а остальное уже дело твоих рук и пальцев');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_site_settings`
--

CREATE TABLE IF NOT EXISTS `dp_site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(255) NOT NULL COMMENT 'Опция',
  `value` text NOT NULL COMMENT 'Значение',
  `name` varchar(255) NOT NULL COMMENT 'Опция (uppercase)',
  `activity` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Включена',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `dp_site_settings`
--

INSERT INTO `dp_site_settings` (`id`, `option`, `value`, `name`, `activity`) VALUES
(1, 'Название сайта', 'Интернет - магазин', 'SITE_NAME', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dp_title_property_group`
--

CREATE TABLE IF NOT EXISTS `dp_title_property_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Имя группы свойств',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `dp_title_property_group`
--

INSERT INTO `dp_title_property_group` (`id`, `name`) VALUES
(1, 'Дисплей'),
(2, 'Фотокамера'),
(3, 'Процессор'),
(4, 'Память');

-- --------------------------------------------------------

--
-- Структура таблицы `dp_user`
--

CREATE TABLE IF NOT EXISTS `dp_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL COMMENT 'Логин',
  `password` varchar(255) NOT NULL COMMENT 'Пароль',
  `role` int(1) NOT NULL DEFAULT '0' COMMENT 'Роль [0-Клиент,  1-Админ,  2-Контент-менеджер]',
  `name` varchar(255) NOT NULL COMMENT 'Имя',
  `surname` varchar(255) NOT NULL COMMENT 'Фамилия',
  `email` varchar(255) NOT NULL,
  `address` text NOT NULL COMMENT 'Адрес',
  `phone` varchar(255) NOT NULL COMMENT 'Телефон',
  `date_add` date NOT NULL COMMENT 'Дата регистрации',
  `birthday` date NOT NULL COMMENT 'Дата рождения',
  `activity` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Активен (подтверждена регистрация)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `dp_user`
--

INSERT INTO `dp_user` (`id`, `login`, `password`, `role`, `name`, `surname`, `email`, `address`, `phone`, `date_add`, `birthday`, `activity`) VALUES
(1, 'admin', 'admin', 1, 'admin', '', '', '', '', '0000-00-00', '0000-00-00', 1),
(2, 'ivan@mail.ru', '12345', 0, 'Иван', 'Иванович', 'ivan@mail.ru', 'г.Ижевск, ул.Азина, д.5, кв.15', '89124123741', '2017-02-20', '1994-01-10', 1),
(3, 'vlad', 'vlad', 0, 'Влад', 'выаыв', 'fdsvdv@ddv.ru', 'Ижевск', '89123433554', '2017-02-22', '1992-02-17', 0),
(4, 'pavel', 'pavel', 0, 'Павел', 'Павлович', 'pavel@mail.ru', 'Москва', '84346328362', '2022-02-20', '1995-04-14', 0),
(5, 'oleg', 'oleg', 0, 'Олег', '', 'sdfsd@dfsd.ru', 'dfg', '3423432', '2022-02-20', '0000-00-00', 0),
(6, 'sdf', 'dsfd', 0, 'fdf', 'sdf', 'dsfs@dsf.df', 'fdf', '231232', '2017-02-22', '2017-02-13', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
