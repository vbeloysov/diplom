<div class="container">
  <form action="index.php?page=reg" class="form-signin" role="form" method="post">
    <h2 class="form-signin-heading">Регистрация</h2>
    <input name="login" type="text" class="form-control" size="15" maxlength="15" placeholder="Логин" required autofocus="">
    <input name="password" type="password" class="form-control" size="15" maxlength="15" placeholder="Пароль" required>
    <input name="name" type="text" class="form-control" size="15" maxlength="15" placeholder="Имя" required>
    <input name="surname" type="text" class="form-control" size="15" maxlength="25" placeholder="Фамилия">
    <input name="email" type="email" class="form-control" size="15" maxlength="25" placeholder="Email" required>
    <input name="address" type="text" class="form-control" size="15" maxlength="35" placeholder="Адрес" required>
    <input name="phone" type="tel" class="form-control" size="15" maxlength="15" placeholder="Телефон" required>
    <!-- <label class="checkbox">
      <input type="checkbox" value="remember-me"> Запомнить меня
    </label> -->
    <input class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="Зарегистрироваться">
  </form>
</div>
