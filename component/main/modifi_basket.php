<?php
  session_start();

  switch ($_POST['action']) {
    case 'add':
      $_SESSION['goods_count'] += $_POST['count'];
      $_SESSION['goods_cost'] += $_POST['count'] * $_POST['price'];
      $_SESSION['goods'][$_POST['product']]['product'] = $_POST['product'];
      $_SESSION['goods'][$_POST['product']]['price'] = $_POST['price'];
      $_SESSION['goods'][$_POST['product']]['count'] += $_POST['count'];
      break;

    case 'delete':
      $_SESSION['goods_count'] -= $_SESSION['goods'][$_POST['product']]['count'];
      $_SESSION['goods_cost'] -= $_SESSION['goods'][$_POST['product']]['count'] * $_SESSION['goods'][$_POST['product']]['price'];
      unset($_SESSION['goods'][$_POST['product']]);
      break;

    case'plus':
      $_SESSION['goods_count'] += 1;
      $_SESSION['goods_cost'] += $_SESSION['goods'][$_POST['product']]['price'];
      $_SESSION['goods'][$_POST['product']]['count'] += 1;
      break;

    case'minus':
      $_SESSION['goods_count'] -= 1;
      $_SESSION['goods_cost'] -= $_SESSION['goods'][$_POST['product']]['price'];
      $_SESSION['goods'][$_POST['product']]['count'] -= 1;
      if($_SESSION['goods'][$_POST['product']]['count'] <= 0)
      {
        unset($_SESSION['goods'][$_POST['product']]);
      }
      break;
  }



  header("Location: ".$_SERVER['HTTP_REFERER']);//редирект на предыдущую страницу
?>
