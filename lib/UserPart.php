<?php
include('DataBase.php');
//Класс для работы клиентской части
class userPart
{
  //функция печати массива данных
  function printArray($dataset) {
    echo "<pre>";
    print_r($dataset); echo
    "</pre>";
  }

  //Функция построения дерева из массива данных (неограниченная вложенность)
  function getTree($dataset) {
  	$tree = array();

  	foreach ($dataset as $id => &$node) {
  		//Если нет вложений
  		if (!$node['parent_id']){
  			$tree[$id] = &$node;
  		}
      else
      {
  			//Если есть потомки то перебераем массив
        $dataset[$node['parent_id']]['childs'][$id] = &$node;
  		}
  	}
  	return $tree;
  }

  //Функция получения меню
  function getMenu() {
    $db = new dataBase();
    $db->connect();

    $sql = "SELECT * FROM dp_page ORDER BY sort";
    $res = mysql_query($sql) or die(mysql_error());

    //Создаем масив где ключ массива является ID меню
  	$menu = array();
  	while($row = mysql_fetch_assoc($res))
    {
      $menu[$row['id']] = $row;
  	}

    $menuTree = $this->getTree($menu);//строим дерево из категорий
    $db->disconnect();
  	return $menuTree;
  }

  //функция получения всех вложенных подкатегорий у категории (не работает!!!!!!!!)
  function getCategoryChilds() {//массив категорий, id родительской категории, итоговый массив id-шников вложенных категорий
    $db = new dataBase();
    $db->connect();


    foreach($categoryItems as $item)
    {
      if($item['id'] == $catId || $repeated)
      {
        $catChilds[] = $item['id'];
        $this->printArray($catChilds);
      }

      if($item['id'] == $catId)
      {

        //$this->printArray($item);
        if(isset($item['childs']))
        {

          $this->getCategoryChilds($item['childs'], $catId, $catChilds, 1);
        }
      }

      if(isset($item['childs']) && $repeated)
      {

        //$this->printArray($catChilds);
        $this->getCategoryChilds($item['childs'], $catId, $catChilds, 1);
      }

      if(isset($item['childs']) && !$repeated)
      {
        //$this->printArray($catChilds);
        $this->getCategoryChilds($item['childs'], $catId, $catChilds, 0);
      }
      //$this->printArray($catChilds);
    }

    return $catChilds;
  }

  //Функция получения категории
  function getCategory() {
    $db = new dataBase();
    $db->connect();

    $sql = "SELECT * FROM dp_category ORDER BY sort";
    $res = mysql_query($sql) or die(mysql_error());

    //Создаем масив где ключ массива является ID категории
  	$cat = array();

  	while($row = mysql_fetch_assoc($res))
    {
      $cat[$row['id']] = $row;
  	}
    $catTree = $this->getTree($cat);//строим дерево из категорий
    $db->disconnect();
    //echo json_encode($catTree);

  	return $catTree;
  }

  //Функция вывода категорий
  function viewCategory($categoryItems, $lvl, $cat) {
    $pageId = $_GET["page"];
    //$lvl - уровень вложенности (изначально при вызове задаем 1)
    foreach($categoryItems as $item)
    {
      if($item["menu_display"])//выводить в меню?
      {

        ?>
          <li><a href="index.php?page=2&cat=<?=$item["id"]?>" class="list-group-item <?if($cat == $item["id"]) echo 'active';?>"><?=$item["title"]?></a></li>
        <?
          if(isset($item["childs"]))//есть вложенные страницы?
          {
            ++$lvl;
            ?>
              <ul class="lvl-<?=$lvl?>">
            <?
              $this->viewCategory($item["childs"], $lvl, $cat);
            ?>
              </ul>
            <?
            $lvl=1;
          }
      }
    }
  }

  //Функция получения изображений товаров
  function getProductImges($imgId) {
    $db = new dataBase();
    $db->connect();

    $sql = "SELECT * FROM dp_product_image WHERE product_id=".$imgId;
    $res = mysql_query($sql) or die(mysql_error());

    $imges = array();
    while($row = mysql_fetch_assoc($res))
    {
      $imges[$row['id']] = $row;
  	}

    $db->disconnect();
  	return $imges;
  }

  //Функция получения отзывов товара
  function getReview($productId) {
    $db = new dataBase();
    $db->connect();

    $sql = "SELECT * FROM dp_review WHERE product_id=".$productId;
    $res = mysql_query($sql) or die(mysql_error());

    $review = array();
    while($row = mysql_fetch_assoc($res))
    {
      $review[] = $row;
  	}

    return $review;
    $db->disconnect();
  }

  //Функция получения пользовательских свойств товара
  function getUserProperty($productId) {
    $db = new dataBase();
    $db->connect();

    //значение свойств
    $sql = "SELECT * FROM dp_product_user_property WHERE product_id=".$productId;
    $res = mysql_query($sql) or die(mysql_error());

    //свойства категории
    $sql2 = "SELECT * FROM dp_category_user_property ORDER BY title_property_group_id";
    $res2 = mysql_query($sql2) or die(mysql_error());

    //заголовки категорий
    $sql3 = "SELECT * FROM dp_title_property_group";
    $res3 = mysql_query($sql3) or die(mysql_error());

    $propertyValue = array();//значение свойств
    $propertyCategory = array();//свойства категории
    $propertyTitle = array();//заголовки категорий

    while($row = mysql_fetch_assoc($res))
    {
      $propertyValue[$row['category_user_property_id']] = $row;
  	}

    while($row2 = mysql_fetch_assoc($res2))
    {
      $propertyCategory[$row2['id']] = $row2;
  	}

    while($row3 = mysql_fetch_assoc($res3))
    {
      $propertyTitle[$row3['id']] = $row3;
  	}

    // $this->printArray($propertyTitle);
    // $this->printArray($propertyCategory);
    // $this->printArray($propertyValue);


    $userProperty = array();
    foreach ($propertyTitle as $titleId => $title) {
      $userProperty['all_propertys'][$titleId]['property_section'] = $title['name'];
      foreach ($propertyCategory as $propertyId => $property) {
        if($property['title_property_group_id'] == $titleId)
        {
          //общий массив свойств
          $userProperty['all_propertys'][$titleId]['propertys'][$propertyId]['name'] = $property['name'];
          $userProperty['all_propertys'][$titleId]['propertys'][$propertyId]['type_view'] = $property['type_view'];
          $userProperty['all_propertys'][$titleId]['propertys'][$propertyId]['value'] = $propertyValue[$propertyId]['value'];

          //массив главных свойств
          if($property['main_property'] == 1)
          {
            $userProperty['main_propertys'][$propertyId]['name'] = $property['name'];
            $userProperty['main_propertys'][$propertyId]['type_view'] = $property['type_view'];
            $userProperty['main_propertys'][$propertyId]['value'] = $propertyValue[$propertyId]['value'];
          }
        }
      }
    }
    return $userProperty;
    $db->disconnect();
  }

  //Функция получение каталога
  function getCatalog($cat) {
    $db = new dataBase();
    $db->connect();

    $sql_part ="";
    if($cat) $sql_part = "WHERE category_id = ".$cat;

    $sql = "SELECT * FROM dp_product ".$sql_part." ORDER BY sort";
    $res = mysql_query($sql) or die(mysql_error());

    $products = array();
    while($row = mysql_fetch_assoc($res))//заполняем все товары данными
    {
      $products[$row['id']] = $row;//исходные данные товара
      $products[$row['id']]['images'] = $this->getProductImges($row['id']);//получаем картинки товара
    }

    $db->disconnect();
    return $products;
  }

  //Функция вывода каталога
  function viewCatalog($catalogItems,$rowProductCount) {
    $i=0;
    $column = 12 / $rowProductCount;//размер калонки
    $clouse = 1;//проверяет все ли дивы закрыты
    $countAddBasket = 1;//Колличество товара добавляемое в корзину
    echo "<div class='bl_catalog'>";
    echo "<h4>Каталог</h4>";
    foreach($catalogItems as $item)
    {
      $i++;
      if($i==1)
      {
        echo "<div class='row'>";
        $clouse = 0;
      }

      $image_main_id= $item['image_main_id'];//id главного изображения
      ?>
        <div class="col-md-<?=$column?> product">
          <a href="index.php?page=8&id=<?=$item['id']?>"><img src="<?=$item['images'][$image_main_id]['image_url']?>" alt="<?=$item['title']?>"></a>
          <a href="index.php?page=8&id=<?=$item['id']?>" class="title"><?=$item['title']?></a>
          <div class="wrap-price">
            <span class="old-price"><?=$item['old_price']?></span>
            <span class="price"><?=$item['price']?></span>
          </div>
          <form action="<?="http://".$_SERVER['SERVER_NAME']."/component/main/modifi_basket.php"?>" method="post">
            <input name="action" type="hidden" value="add"/>
            <input name="count" type="hidden" value="<?=$countAddBasket?>"/>
            <input name="price" type="hidden" value="<?=$item['price']?>"/>
            <input name="product" type="hidden" value="<?=$item['id']?>"/>
            <input type="submit" value="Купить"/>
          </form>

        </div>
      <?

      if($i==$rowProductCount)
      {
        echo "</div>";
        $i=0;
        $clouse = 1;
      }
    }

    if(!$clouse)
    {
      echo "</div>";
    }
    echo "</div><!--end catalog-->";
  }

  //Функция получения всех данных продукта
  function getProduct($id) {
    $db = new dataBase();
    $db->connect();

    $sql = "SELECT * FROM dp_product  WHERE id=".$id;
    $res = mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_assoc($res);
    $product = $row;
    $product['images'] = $this->getProductImges($row['id']);//получаем картинки товара
    $product['reviews'] = $this->getReview($row['id']);//получаем отзывы товара
    $product['user_propertys'] = $this->getUserProperty($row['id']);//получаем пользовательские свойтва товара

    return $product;
    $db->disconnect();
  }

  //Функция вывода продукта
  function viewProduct($productItems) {
    ?>
    <h4><?=$productItems['title']?></h4>
    <div class="bl_product">
      <div class="row">
        <div class="photo col-md-6">
          <img src="<?=$productItems['images'][$productItems['image_main_id']]['image_url']?>" alt="<?=$productItems['title']?>">
          <div class="photo-preview">
            <?
              foreach ($productItems['images'] as $image)
              {
                ?>
                  <a href="#"><img src="<?=$image['image_url']?>" alt="<?=$productItems['title']?>"></a>
                <?
              }
            ?>
          </div>
          <div class="buy">
            <div class="wrap-price">
              <span class="old-price"><?=$productItems['old_price']?></span>
              <span class="price"><?=$productItems['price']?></span>
            </div>
            <p><span>Остаток товара: </span><?if($productItems['count']<0){echo "Много";}else{echo $productItems['count']." шт.";} ?></p>
            <a href="#" class="btn-buy">Купить</a>
          </div>
        </div>
        <div class="properties-main col-md-6">
          <h4>Основные совйства:</h4>
            <p><span class="title-properties">Артикул: </span><?=$productItems['number_code']?></p>
            <?
                foreach ($productItems['user_propertys']['main_propertys'] as $mainProperty)
                {
                  if(isset($mainProperty['value']))
                  {
                    ?>
                      <p><span class="title-properties"><?=$mainProperty['name']?>: </span><?=$mainProperty['value']?></p>
                    <?
                  }
                }
            ?>
        </div>
      </div><!-- /row -->

      <!-- описание -->
      <div class="description">
        <h4>Описание:</h4>
        <p><?=$productItems['html_content']?></p>
      </div>

      <div class="propertys-detail">
        <h4>Свойства:</h4>
        <?
            foreach ($productItems['user_propertys']['all_propertys'] as $allProperty)
            {
              if(isset($allProperty['propertys']))
              {
                ?>
                <div class="propertys-detail-section">
                  <h5><?=$allProperty['property_section']?>:</h5>
                  <?
                    foreach ($allProperty['propertys'] as $property)
                    {
                      if(isset($property['value']))
                      {
                        ?>
                          <p><span class="title-properties"><?=$property['name']?>: </span><?=$property['value']?></p>
                        <?
                      }
                    }
                  ?>
                </div>
                <?
              }
            }
        ?>
      </div><!-- propertys-detail -->

      <!-- отзывы -->
      <div class="reviews">
        <h4>Отзывы:</h4>
        <?
          if (!isset($productItems['reviews'][0]))
          {
            echo "<p>Нет отзывов</p>";
          }
          else
          {
            foreach ($productItems['reviews'] as $review)
            {


              ?>
                <div class="el">
                  <span class="author"><?=$review['author']?>, <?=$review['review_date']?></span>
                  <p><?=$review['review']?></p>
                </div>
              <?
            }
          }
        ?>
      </div><!-- reviews -->
    </div><!-- bl_product -->
    <?
  }

  //Функция получения данных текущей страницы
  function getPage($pageId) {
    $db = new dataBase();
    $db->connect();
    $sql = "SELECT * FROM dp_page WHERE id=".$pageId." ORDER BY sort";
    $res = mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_assoc($res);

    $sql2 = "SELECT * FROM dp_layout WHERE id=".$row['loyout_id'];
    $res2 = mysql_query($sql2) or die(mysql_error());
    $row2 = mysql_fetch_assoc($res2);

    $sql3 = "SELECT * FROM dp_page_component WHERE page_id=".$pageId." ORDER BY component_cell";
    $res3 = mysql_query($sql3) or die(mysql_error());

    $sql4 = "SELECT * FROM dp_component";
    $res4 = mysql_query($sql4) or die(mysql_error());

    $sql5 = "SELECT * FROM dp_layout_component WHERE loyout_id=".$row['loyout_id']." ORDER BY component_cell";
    $res5 = mysql_query($sql5) or die(mysql_error());

    $component = array();
    while($row4 = mysql_fetch_assoc($res4))
    {
      $component[$row4['id']] = $row4;
    }


  	$page = $row;
    $page['layout_url']=$row2['url'];
    $page['quantity_component']=$row2['quantity_component'];

    //Компоненты шаблона
    while($row5 = mysql_fetch_assoc($res5))
    {
      $page['component'][$row5['component_cell']] = $row5;
      $page['component'][$row5['component_cell']]['name'] = $component[$row5['component_id']]['name'];
      $page['component'][$row5['component_cell']]['url'] = $component[$row5['component_id']]['url'];
    }

    //Компоненты страницы
    while($row3 = mysql_fetch_assoc($res3))
    {
      $page['component'][$row3['component_cell']] = $row3;
      $page['component'][$row3['component_cell']]['name'] = $component[$row3['component_id']]['name'];
      $page['component'][$row3['component_cell']]['url'] = $component[$row3['component_id']]['url'];
    }



    $db->disconnect();
  	return $page;
  }

  //Функция вывода страницы
  function viewPage($pageItems) {
    ?>
        <p><?=$pageItems['html_content']?></p>
    <?
  }

  //Функция получения настроек сайта
  function getSiteSettings() {
    $db = new dataBase();
    $db->connect();
    $sql = "SELECT * FROM dp_site_settings";
    $res = mysql_query($sql) or die(mysql_error());
    $settings = array();
    while($row = mysql_fetch_assoc($res))
    {
      if($row['activity'])
      {
        $settings[$row['id']] = $row;
      }
    }
    $db->disconnect();
  	return $settings;
  }

  //Функция получения переменной из массива POST
  function getVariable($variable) {
    if(isset($variable) && $variable!='')
    {
      $variable = stripslashes($variable);
      $variable = htmlspecialchars($variable);
      $variable = trim($variable);
    }
    return $variable;
  }

  //Функция проверки введенного логина и пароля (Авторизация)
  function checkAuth($login, $password) {
    $db = new dataBase();
    $db->connect();
    $sql = "SELECT * FROM dp_user WHERE login='".$login."'"; //извлекаем из базы все данные о пользователе с введенным логином
    $res = mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_assoc($res);
    if (empty($row['password']))
    {
      //если пользователя с введенным логином не существует
      exit ("Извините, введённый вами login или пароль неверный.");
    }
    else
    {
      //если существует, то сверяем пароли
      if ($row['password']==$password)
      {
        //если пароли совпадают, то запускаем пользователю сессию! Можете его поздравить, он вошел!
        $_SESSION['login']=$row['login'];
        $_SESSION['id']=$row['id'];
        $_SESSION['role']=$row['role'];
        //echo "Вы успешно вошли на сайт!";
        //return 0;
      }
      else
      {
        //если пароли не сошлись
        exit ("Извините, введённый вами login или пароль неверный.");
      }
    }
    $db->disconnect();
    return 0;
  }

  //Функция вставки в бд нового пользователя
  function insertUser($login,$password,$name,$email,$address,$phone,$surname) {
    $db = new dataBase();
    $db->connect();
    $sql = "SELECT id FROM dp_user WHERE login='$login'";
    $res = mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_array($res);
    if (!empty($row['id']))
    {
      exit ("Извините, введённый вами логин уже зарегистрирован. Введите другой логин.");
    }
    // если такого нет, то сохраняем данные
    $date_add = date('Y-m-d');
    $sql2 = "INSERT INTO dp_user (login,password,role,name,surname,email,address,phone,date_add) VALUES('$login','$password','0','$name','$surname','$email','$address','$phone','$date_add')";
    $res2 = mysql_query($sql2) or die(mysql_error());
    // Проверяем, есть ли ошибки
    if ($res2=='TRUE')
    {
      echo "Вы успешно зарегистрированы! <a href='index.php?page=1'>Авторизуйтесь на сайте</a>";
    }
    else
    {
      echo "Ошибка! Вы не зарегистрированы.";
    }
    $db->disconnect();
    return 0;
  }

  //Функция вывода формы авторизации
  function viewAuth() {
    ?>
    <div class="auth">
      <form action="<?=$url?>" class="" role="form" method="post">
        <span>Логин: </span><input name="login" type="text" class="" size="15" maxlength="15" placeholder="Логин" required="" autofocus=""><br/>
        <span>Пароль: </span><input name="password" type="password" class="" size="15" maxlength="15" placeholder="Пароль" required="">
        <input class="" type="submit" name="submit" value="Войти">
        <a href="<?="http://".$_SERVER['SERVER_NAME'].'/index.php?page=reg'?>">Регистрация</a>
      </form>
    </div>
    <?
    return 0;
  }

  //Функция получения всех элементов большой корзины
  function getBasketBig() {
    //$this->printArray($_SESSION);
    $basketBigItems = array();
    foreach ($_SESSION['goods'] as $productId => $value)
    {
      $basketBigItems[$productId] = $this->getProduct($productId);
    }
    return $basketBigItems;
  }

  //Функция вывода большой корзины
  function viewBasketBig($basketBigItems) {
    $urlDeleteBasket = "http://".$_SERVER['SERVER_NAME'].'/component/main/delete_basket.php';
    echo "<div class='basket'>";
    if($_SESSION['goods_count'] > 0)
    {
      foreach ($basketBigItems as $key => $value) {
        ?>
          <div class="row basket-item">
            <div class="col-md-3">
              <a href="/index.php?page=8&id=<?=$value['id']?>" class=""><img src="<?=$value['images'][$value['image_main_id']]['image_url']?>" alt="<?=$value['title']?>"></a>
            </div>
            <div class="col-md-4">
              <a href="/index.php?page=8&id=<?=$value['id']?>" class=""><?=$value['title']?></a>
            </div>
            <div class="col-md-2">
              <form action="<?="http://".$_SERVER['SERVER_NAME']."/component/main/modifi_basket.php"?>" method="post">
                <input name="action" type="hidden" value="minus"/>
                <input name="product" type="hidden" value="<?=$key?>"/>
                <input type="submit" class="count-button plus" value="-"/>
              </form>
              <span><?=$_SESSION['goods'][$key]['count']?></span>
              <form action="<?="http://".$_SERVER['SERVER_NAME']."/component/main/modifi_basket.php"?>" method="post">
                <input name="action" type="hidden" value="plus"/>
                <input name="product" type="hidden" value="<?=$key?>"/>
                <input type="submit" class="count-button plus" value="+"/>
              </form>
            </div>
            <div class="col-md-3">
              <p><?=$value['price']*$_SESSION['goods'][$key]['count']."руб."?></p>
              <? if($_SESSION['goods'][$key]['count'] > 1) {?>
                <p><?=$value['price']."руб. "." x ".$_SESSION['goods'][$key]['count']."шт."?></p>
              <? } ?>

              <form action="<?="http://".$_SERVER['SERVER_NAME']."/component/main/modifi_basket.php"?>" method="post">
                <input name="action" type="hidden" value="delete"/>
                <input name="product" type="hidden" value="<?=$key?>"/>
                <input type="submit" value="Удалить"/>
              </form>
            </div>
          </div>
        <?
      }
      ?>
        <div class="row basket-item">
          <div class="col-md-3"></div>
          <div class="col-md-4">
            <h4>ИТОГО</h4>
          </div>
          <div class="col-md-2">
            <?=$_SESSION['goods_count']."шт."?>
          </div>
          <div class="col-md-3">
            <?=$_SESSION['goods_cost']."руб."."</br>"?>
            <a href="/index.php?page=10">Оформить заказ</a>
          </div>
        </div>

      <?
    }
    else
    {
      echo "<p>Корзина пуста!</p>";
    }

    echo "</div>";
  }

  //Функция получения данных пользователя
  function getUser($userId) {
    $db = new dataBase();
    $db->connect();
    $sql = "SELECT * FROM dp_user WHERE id=".$userId;
    $res = mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_assoc($res);
    $db->disconnect();
    return $row;
  }

  //Функция получения всех способов доставки
  function getDelivery() {
    $db = new dataBase();
    $db->connect();
    $sql = "SELECT * FROM dp_delivery";
    $res = mysql_query($sql) or die(mysql_error());

    $delivery = array();
    while($row = mysql_fetch_assoc($res))
    {
      if($row['activity'])
      {
        $delivery[$row['id']] = $row;
      }
    }
    $db->disconnect();
    return $delivery;
  }

  //Функция получения способов оплаты
  function getPayment() {
    $db = new dataBase();
    $db->connect();
    $sql = "SELECT * FROM dp_payment";
    $res = mysql_query($sql) or die(mysql_error());

    $payment = array();
    while($row = mysql_fetch_assoc($res))
    {
      if($row['activity'])
      {
        $payment[$row['id']] = $row;
      }
    }
    $db->disconnect();
    return $payment;
  }

  //Функция получения всех данных заказа
  function getOrder() {
    $orderItems = array();
    $orderItems['order_number'] = '';
    if(isset($_SESSION['id']))
    {
      $userItems = $this->getUser($_SESSION['id']);
    }
    $orderItems['user'] = $userItems;
    $orderItems['order_date'] = date('Y-m-d');
    $orderItems['order_content'] = $_SESSION['goods'];
    $orderItems['order_content_detail'] = $this->getBasketBig();
    $orderItems['order_summ'] = $_SESSION['goods_cost'];
    $orderItems['goods_count'] = $_SESSION['goods_count'];
    $orderItems['delivery_methods'] = $this->getDelivery();
    $orderItems['payment_methods'] = $this->getPayment();


    return $orderItems;
  }

  //Функция вывода формы оформления зыказа
  function viewOrder($orderItems) {
    $order_content =  $this->getVariable(serialize($orderItems['order_content']));
    //$order_content = unserialize($order_content);
    //$this->printArray($order_content);
    //$this->printArray($orderItems);

    //$this->printArray($order_content);
    ?>
    <div class="row">
      <form action="<?=$_SERVER['REQUEST_URI']?>" class="orderForm" method="post">
        <div class="col-md-6">

          <?
            if(isset($_SESSION['id']))
            {
          ?>
              <h4>Контактная информация</h4>
              <span class="inputLabel">Имя</span>
              <input name="" type="text" placeholder="Имя" disabled value="<?=$orderItems['user']['name']?>"/>
              <span class="inputLabel">Фамилия</span>
              <input name="" type="text" placeholder="Фамилия" disabled value="<?=$orderItems['user']['surname']?>"/>
              <span class="inputLabel">Email</span>
              <input name="" type="text" placeholder="Email" disabled value="<?=$orderItems['user']['email']?>"/>
              <span class="inputLabel">Телефон</span>
              <input name="" type="text" placeholder="Телефон" disabled value="<?=$orderItems['user']['phone']?>"/>
              <span class="inputLabel">Адрес</span>
              <input name="" type="text" placeholder="Адрес" disabled value="<?=$orderItems['user']['address']?>"/>
              <a href="#">Изменить личные данные</a>
          <?
            }
            else
            {
          ?>
              <h4>Регистрация на сайте</h4>
              <span class="inputLabel">Имя</span>
              <input name="name" type="text" placeholder="Имя" value=""/>
              <span class="inputLabel">Фамилия</span>
              <input name="surname" type="text" placeholder="Фамилия" value=""/>
              <span class="inputLabel">Email</span>
              <input name="email" type="text" placeholder="Email" value=""/>
              <span class="inputLabel">Телефон</span>
              <input name="phone" type="text" placeholder="Телефон" value=""/>
              <span class="inputLabel">Адрес</span>
              <input name="address" type="text" placeholder="Адрес" value=""/>
              <span class="inputLabel">Пароль для сайта</span>
              <input name="password" type="text" placeholder="Пароль" value=""/>
          <?
            }
          ?>
            <input name="user_id" type="hidden"  value="<?=$orderItems['user']['id']?>"/>
            <input name="order_summ" type="hidden"  value="<?=$orderItems['order_summ']?>"/>
            <input name="order_content" type="hidden"  value="<?=$order_content?>"/>

          <h4>Способ доставки</h4>
            <? foreach ($orderItems['delivery_methods'] as $key => $value) { ?>
            <div class="order-list-item">
              <input name="delivery" id="delivery<?=$key?>" value="<?=$key?>" type="radio" class="radio">
              <label name="delivery" for="delivery<?=$key?>">
                <p><?=$value['name']?></p>
                <p><?=$value['cost']."руб."?></p>
                <p><?=$value['html_content']?></p>
              </label>
            </div>
            <? } ?>
          <h4>Способ оплаты</h4>
            <? foreach ($orderItems['payment_methods'] as $key => $value) { ?>
            <div class="order-list-item">
              <input name="payment" id="payment<?=$key?>" value="<?=$key?>" type="radio" class="radio">
              <label name="payment" for="payment<?=$key?>">
                <p><?=$value['name']?></p>
              </label>
            </div>
            <? } ?>

        </div>
        <div class="col-md-6 order-list">
          <h4>Состав заказа</h4>
          <? foreach ($orderItems['order_content_detail'] as $key => $value) { ?>
            <div class="row order-list-item">
              <div class="col-md-2">
                <a href="/index.php?page=8&id=<?=$value['id']?>" class=""><img src="<?=$value['images'][$value['image_main_id']]['image_url']?>" alt="<?=$value['title']?>"></a>
              </div>
              <div class="col-md-5">
                <a href="/index.php?page=8&id=<?=$value['id']?>" class=""><?=$value['title']?></a>
              </div>
              <div class="col-md-1">
                <?=$orderItems['order_content'][$key]['count']."шт."?>
              </div>
              <div class="col-md-4">
                <p><?=$orderItems['order_content'][$key]['price'] * $orderItems['order_content'][$key]['count']."руб."?></p>
                <? if($orderItems['order_content'][$key]['count'] > 1) {?>
                  <p><?=$orderItems['order_content'][$key]['price']."руб. "." x ".$orderItems['order_content'][$key]['count']."шт."?></p>
                <? } ?>
              </div>
            </div>
          <? } ?>
          <textarea rows="10" cols="45" name="comment"></textarea>
          <input name="order" type="submit" class="" value="Оформить заказ"/>
        </div>
      </form>
    </div><!--row-->



    <?
    return 0;
  }

  //Функция очистки корзины
  function basketCleaning() {
    foreach ($_SESSION as $key => $value) {
      if($key!='login' && $key!='id' && $key!='role')
        unset($_SESSION[$key]);
    }
    return 0;
  }

  //Фукция возвращает id последней записи в таблице
  function getLineLast($table) {
    $db = new dataBase();
    $db->connect();
    $sql = "SELECT * FROM $table ORDER BY id DESC LIMIT 1";
    $res = mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_assoc($res);
    return $row['id'];
  }

  //Функция вставки нового статуса заказа (при формировании нового заказа)
  function insertOrderStatus($order_id) {
    $db = new dataBase();
    $db->connect();
    $date = date('Y-m-d');
    $sql = "INSERT INTO dp_order_status (id,order_id,order_status,order_status_date) VALUES('','$order_id','','$date')";
    $res = mysql_query($sql) or die(mysql_error());

    $db->disconnect();
    return 0;
  }

  //Функция вставки в бд нового заказа
  function insertOrder() {
    $db = new dataBase();
    $db->connect();

    if(!isset($_SESSION['id']))
    {
      $password=$this->getVariable($_POST['password']);
      $name=$this->getVariable($_POST['name']);
      $email=$this->getVariable($_POST['email']);
      $address=$this->getVariable($_POST['address']);
      $phone=$this->getVariable($_POST['phone']);
      $surname=$this->getVariable($_POST['surname']);
      $login = $email;
      $this->insertUser($login,$password,$name,$email,$address,$phone,$surname);
      $user_id = $this->getLineLast('dp_user');//id текущего пользователя
      //авторизуем пользователя
      $_SESSION['login']=$login;
      $_SESSION['id']=$user_id;
      $_SESSION['role']=0;
    }
    else
    {
      $user_id=$this->getVariable($_POST['user_id']);
    }


    $order_summ=$this->getVariable($_POST['order_summ']);
    $delivery_id=$this->getVariable($_POST['delivery']);
    $payment_id=$this->getVariable($_POST['payment']);
    $comment=$this->getVariable($_POST['comment']);
    $order_content = $_POST['order_content'];

    $sql3 = "SELECT * FROM dp_delivery WHERE id='$delivery_id'";
    $res3 = mysql_query($sql3) or die(mysql_error());
    $row3 = mysql_fetch_assoc($res3);
    $delivery_cost=$row3['cost'];

    //Если пользователь существует
    if(isset($_SESSION['id']))
    {
      $order_date = date('Y-m-d');
      $sql = "INSERT INTO dp_order (order_number,user_id,order_date,order_status,order_content,order_summ,delivery_id,delivery_cost,payment_id,comment) VALUES('','$user_id','$order_date','','$order_content','$order_summ','$delivery_id','$delivery_cost','$payment_id','$comment')";
      $res = mysql_query($sql) or die(mysql_error());

      $order_id = $this->getLineLast('dp_order');//id текущего заказа
      if($res=='TRUE')
      {
        $this->insertOrderStatus($order_id);//устанавливаем статус заказа по умолчанию (не подтвержден)
        $order_status_id = $this->getLineLast('dp_order_status');//ссылка на текущий статус заказа
        $sql2 = "UPDATE dp_order SET order_status='$order_status_id' WHERE id=$order_id";
        $res2 = mysql_query($sql2) or die(mysql_error());
      }

    }

    // Проверяем, есть ли ошибки
    if($res=='TRUE' && $res2=='TRUE')
    {
      $this->basketCleaning();

      echo "Ваш заказ принят! Вы можете отслеживать его вополнение в <a href='#'>Личном кабинете</a>";
    }
    else
    {
      echo "Ошибка при отправке заказа!";
    }

    $db->disconnect();
    return 0;
  }



}
?>
